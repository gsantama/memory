/**
 * <h2>Memory</h2>
 * 
 * @version 1
 * @author Institut Sabadell
 * 
 * CFGS DAM, DAMvi
 * M03UF2, Programaci� Estructurada
 * 
 * Versi� del popular joc del Memory
 * Sobre un tauler de 4x4
 * Implementa la possibilitat de joc individual o entre dos jugadors
 */

package memory;

import java.util.Scanner;

public class MemoryS4doc {
	public static final int TAULER = 4;		//final vol dir que un cop li assignes un valor, no pot canviar. �s una constant
	public static Scanner sc = new Scanner(System.in);		

	public static void main(String[] args) {
		//procurem que el main sigui el m�s polit possible
		char[][] Tauler = new char[TAULER][TAULER]; // representa el tauler que veu l'usuari durant la partida
		char[][] Secret = new char[TAULER][TAULER]; // representa el tauler que la soluci� del joc, i que cal anar descobrint.
		int Opcio;
		System.out.println("Benvinguts al joc MEMORY!");
		System.out.println();
		do {
			Opcio = Menu();
			switch (Opcio) {
			default:
			case 0:
				break;

			case 1:
				inicialitzarTauler(Tauler);
				posarPeces(Secret);
				remenarPeces(Secret);
				partidaSolitaria(Tauler, Secret);
				break;

			case 2:
				inicialitzarTauler(Tauler);
				posarPeces(Secret);
				remenarPeces(Secret);
				partidaNormal(Tauler, Secret);
				//partidaNormalV2(Tauler, Secret);		//una altra manera d'implementar el joc, �s operativa
				break;
			}
		} while (Opcio != 0);
	}

	/**
	 * Permet fer una jugada segons la seg�ent l�gica:
	 * i)   mostra el tauler 
	 * ii)  demana primera casella, que ha d'estar tapada
	 * iii) destapa la casella triada
	 * iv)  mostra el tauler
	 * v)   demana una segona casella, que ha d'estar tapada
	 * vi)  destapa la casella triada
	 * vii) mostra el tauler
	 * viii)comprova si �s parella, aleshores retornar� true
	 * ix)  si no �s parella, tapar� les caselles i retornar� false 
	 
	 * @param Matriu tauler que veu l'usuari
	 * @param Secret tauler que cont� la soluci� del joc
	 * @return true si l'usuari fa parella, false cas contrari
	 */
	
	public static boolean ferJugada(char[][] Matriu, char[][] Secret) {
		int filaCasella1, columnaCasella1;
		int filaCasella2, columnaCasella2;
		char valor1, valor2;

		mostrarTauler(Matriu);
		// obtenir la primera casella correcta, hem de fer una estructura iterativa
		// perqu� potser l'usuari tria una casella ja destapada
		do {
			System.out.println("Si us plau, indica una casella que estigui tapada");
			filaCasella1 = obtenirFila();
			columnaCasella1 = obtenirColumna();
		} while (validaCasella(Matriu, filaCasella1, columnaCasella1) == false);

		/*
		 * la casella �s correcta, hem de destapar-la: - agafem el valor de la mateixa
		 * posici� a secret - posem el valor obtingut a la corresponent posici� del
		 * tauler
		 */
		valor1 = obtenirCasella(Secret, filaCasella1, columnaCasella1);
		destapaCasella(Matriu, filaCasella1, columnaCasella1, valor1);

		mostrarTauler(Matriu);

		// obtenir la segona casella correcta, hem de fer una estructura iterativa
		// perqu� potser l'usuari tria una casella ja destapada
		do {
			System.out.println("Si us plau, indica una casella que estigui tapada");
			filaCasella2 = obtenirFila();
			columnaCasella2 = obtenirColumna();
		} while (validaCasella(Matriu, filaCasella2, columnaCasella2) == false);

		/*
		 * la casella �s correcta, hem de destapar-la: - agafem el valor de la mateixa
		 * posici� a secret - posem el valor obtingut a la corresponent posici� del
		 * tauler
		 */
		valor2 = obtenirCasella(Secret, filaCasella2, columnaCasella2);
		destapaCasella(Matriu, filaCasella2, columnaCasella2, valor2);

		mostrarTauler(Matriu);

		// finalment, comprovar formen parella
		if (valor1 == valor2) // parella!!!
			return true;
		else { // no �s parella, hem de desfer la jugada i retornar false
			tapaCasella(Matriu, filaCasella1, columnaCasella1);
			tapaCasella(Matriu, filaCasella2, columnaCasella2);
			return false;
		}
	}

	/**
	 * funci� que calcula el n�mero de caselles tapades
	 * @param Matriu tauler que veu l'usuari
	 * @return retorna el n�mer de caselles que encara estan tapades
	 */
	
	public static int casellesPendents(char[][] Matriu) {
		// comptarem el n�mero de caselles amb el valor '?'

		int caselles = 0;

		for (int i = 0; i < TAULER; i++)
			for (int j = 0; j < TAULER; j++)
				if (Matriu[i][j] == '?')
					caselles++;
		return caselles;
	}


	/**
	 * acci� que permete jugar una partida entre dos jugadors,
	 * controlant els torns i les puntuacions 
	 * 
	 * @param Matriu tauler que veu l'usuari
	 * @param Secret tauler que cont� la soluci� del joc
	 */
	
	public static void partidaNormal(char[][] Matriu, char[][] Secret) {
		// inicialitzaci� de les dades b�siques de la partida
		int torn = 0;
		int puntsJugador1 = 0;
		int puntsJugador2 = 0;

		while (casellesPendents(Matriu) >= 2) {
			if (ferJugada(Matriu, Secret)) {
				System.out.println("PARELLA");
				// incrementem la puntuaci� del jugador que ha aconseguit la parella
				if (torn == 0)
					puntsJugador1++;
				else
					puntsJugador2++;

				System.out.println("Punts Jugador 1: " + puntsJugador1);
				System.out.println("Punts Jugador 2: " + puntsJugador2);
			}

			else{
				System.out.println("UNA ALTRA VEGADA SER�");
				// canvi de torn
				torn=(torn+1)%2;
			}
		}
		System.out.println("Final de la partida.");
		if (puntsJugador1 > puntsJugador2)
			System.out.println("Guanyador Jugador 1");
		else if(puntsJugador2 > puntsJugador1)
			System.out.println("Guanayador Jugador 2");
		else
			System.out.println("Empat");
		
		System.out.println("Punts Jugador 1: " + puntsJugador1);
		System.out.println("Punts Jugador 2: " + puntsJugador2);
				
		return;
	}

	/**
	 * acci� que permete jugar una partida entre dos jugadors,
	 * controlant els torns i les puntuacions 
	 * fa servir una classe auxiliar Partida que cont� les dades b�siques d'una partida
	 * 
	 * @param Matriu tauler que veu l'usuari
	 * @param Secret tauler que cont� la soluci� del joc
	 */
	public static void partidaNormalV2(char[][] Matriu, char[][] Secret) {
		// inicialitzaci� de les dades b�siques de la partida
		Partida.torn = 0;
		Partida.puntsJugador1 = 0;
		Partida.puntsJugador2 = 0;

		while (casellesPendents(Matriu) >= 2) {
			if (ferJugada(Matriu, Secret)) {
				System.out.println("PARELLA");
				// incrementem la puntuaci� del jugador que ha aconseguit la parella
				if (Partida.torn == 0)
					Partida.puntsJugador1++;
				else
					Partida.puntsJugador2++;

				System.out.println("Punts Jugador 1: " + Partida.puntsJugador1);
				System.out.println("Punts Jugador 2: " + Partida.puntsJugador2);
			}

			else{
				System.out.println("UNA ALTRA VEGADA SER�");
				// canvi de torn
				Partida.torn=(Partida.torn+1)%2;
			}
		}
		System.out.println("Final de la partida.");
		if (Partida.puntsJugador1 > Partida.puntsJugador2)
			System.out.println("Guanyador Jugador 1");
		else if(Partida.puntsJugador2 > Partida.puntsJugador1)
			System.out.println("Guanayador Jugador 2");
		else
			System.out.println("Empat");
		
		System.out.println("Punts Jugador 1: " + Partida.puntsJugador1);
		System.out.println("Punts Jugador 2: " + Partida.puntsJugador2);
				
		return;
	}

	/**
	 * acci� que permete jugar una partida amb un �nic jugador
	 * 
	 * @param Matriu tauler que veu l'usuari
	 * @param Secret tauler que cont� la soluci� del joc
	 */
	public static void partidaSolitaria(char[][] Matriu, char[][] Secret) {
		while (casellesPendents(Matriu) >= 2)
			if (ferJugada(Matriu, Secret))
				System.out.println("PARELLA");
			else
				System.out.println("UNA ALTRA VEGADA SER�");

		return;
	}

	/**
	 * acci� que inicialitza el tauler de joc
	 * @param Matriu tauler que veu l'usuari
	 */
	public static void inicialitzarTauler(char[][] Matriu) {
		for (int F = 0; F < TAULER; F++)
			for (int C = 0; C < TAULER; C++)
				Matriu[F][C] = '?';
	}

	/**
	 * acci� que mostra el tauler de joc
	 * @param Matriu tauler que veu l'usuari
	 */
	public static void mostrarTauler(char[][] Matriu) {
		for (int F = 0; F < TAULER; F++) {
			System.out.print(F);
			for (int C = 0; C < TAULER; C++)
				System.out.print(" " + Matriu[F][C]);
			System.out.println();
		}
		System.out.print("  0 1 2 3");
		System.out.println();
	}

	/**
	 * acci� que posa els valors de les caselles a descobrir en un tauler, 
	 * de manera b�sica
	 * @param Matriu tauler on es posen els valors
	 */

	public static void posarPeces(char[][] Matriu) {
		/*
		 * Matriu[0][0] = 'A'; Matriu[0][1] = 'A'; Matriu[0][2] = 66; //'B' ...
		 * (raonable per ser 16 posicions)
		 */

		for (int F = 0; F < TAULER; F++)
			for (int C = 0; C < TAULER; C++)
				if (F == 0)
					if (C == 0 || C == 1)
						Matriu[F][C] = 'A';
					else
						Matriu[F][C] = 'B';
				else if (F == 1)
					if (C == 0 || C == 1)
						Matriu[F][C] = 'C';
					else
						Matriu[F][C] = 'D';
				else if (F == 2)
					if (C == 0 || C == 1)
						Matriu[F][C] = 'E';
					else
						Matriu[F][C] = 'F';
				else if (F == 3)
					if (C == 0 || C == 1)
						Matriu[F][C] = 'G';
					else
						Matriu[F][C] = 'H';
	}

	/**
	 * acci� que remena les caselles d'un tauler
	 * @param Matriu tauler que vol rementar
	 */

	public static void remenarPeces(char[][] Matriu) {
		int Fila, Columna;
		char Aux;

		for (int F = 0; F < TAULER; F++)
			for (int C = 0; C < TAULER; C++) {
				Fila = (int) (Math.random() * TAULER);
				Columna = (int) (Math.random() * TAULER);
				Aux = ' ';

				Aux = Matriu[F][C];
				Matriu[F][C] = Matriu[Fila][Columna];
				Matriu[Fila][Columna] = Aux;
			}
	}

	public static int Menu() {
		int Opcio;
		System.out.println("0. Sortir");
		System.out.println("1. Jugar Partida 'Solit�ria'");
		System.out.println("2. Jugar Partida dos Jugadors");

		do {
			System.out.print("Digues una Opci�: ");
			Opcio = sc.nextInt();
			if (Opcio > 3 || Opcio < 0)
				System.out.println("Opcio no v�lida");

		} while (Opcio > 3 || Opcio < 0);
		System.out.println();
		return Opcio;
	}


	/**
	 * funci� que retorna un valor enter corresponet a una fila
	 * @return fila triada
	 */
	public static int obtenirFila() {
		int fila;
		// obtenir valor de coordenada fila
		do {
			System.out.println("Digem la fila que vols?");
			fila = sc.nextInt();
			if (fila < 0 || fila >= TAULER)
				System.out.println("Si us plau, entre 0 i " + (TAULER - 1));
		} while (fila < 0 || fila >= TAULER);
		return fila;
	};

	/**
	 * funci� que retorna un valor enter corresponet a una columna
	 * @return columna triada
	 */
	public static int obtenirColumna() {
		int columna;
		// obtenir valor de coordenada Y
		do {
			System.out.println("Digem la columna que vols?");
			columna = sc.nextInt();
			if (columna < 0 || columna >= TAULER)
				System.out.println("Si us plau, entre 0 i " + (TAULER - 1));
		} while (columna < 0 || columna >= TAULER);
		return columna;
	}

	/**
	 * funci� que retorna si una casella est� tapada o no
	 * @param matriu  tauler que cont� les caselles
	 * @param fila 	fila de la casella
	 * @param columna columna de la casella
	 * @return true si la casella est� tapada, false cas contrari
	 */	
	public static boolean validaCasella(char[][] matriu, int fila, int columna) {
		return (matriu[fila][columna] == '?');
	}

	/**
	 * funci� que retorna el contingut d'una casella d'un tauler
	 * @param matriu  tauler que cont� les caselles
	 * @param fila 	fila de la casella
	 * @param columna columna de la casella
	 * @return valor de la casella
	 */
	public static char obtenirCasella(char[][] matriu, int fila, int columna) {
		return (matriu[fila][columna]);
	}

	/**
	 * acci� que destapa una casella d'un tauler
	 * @param matriu  tauler que cont� les caselles
	 * @param fila 	fila de la casella
	 * @param columna columna de la casella
	 * @param valor valor que s'ha de posar a la casella a destapar
	 */	
	public static void destapaCasella(char[][] matriu, int fila, int columna, char valor) {
		matriu[fila][columna] = valor;
	}

	/**
	 * acci� que tapa la casella d'un tauler
	 * @param matriu  tauler que cont� les caselles
	 * @param fila 	fila de la casella
	 * @param columna columna de la casella
	 */
	public static void tapaCasella(char[][] matriu, int fila, int columna) {
		matriu[fila][columna] = '?';
	}

	/**
	 * acci� que permet triar una casella d'un tauler verificant si est� tapada i destapant-la
	 * @param tauler  tauler que cont� les caselles
	 * @param secret tauler que cont� la soluci� del joc
	 */
	public static void triaCasella(char[][] tauler, char[][] secret) {
		int fila, columna;
		do {
			fila = obtenirFila();
			columna = obtenirColumna();
			if (!validaCasella(tauler, fila, columna))
				System.out.println("Casella ja destapada. Tria una altra");
		} while (!validaCasella(tauler, fila, columna));

		char valor = obtenirCasella(secret, fila, columna);
		destapaCasella(tauler, fila, columna, valor);
	}

	
	/**
	 * acci� que permet inicialitzar un tauler soluci� d'una manera elegant i independentment del n�mero de la dimensi� del mateix
	 * @param Matriu  tauler on deixarem els valors de la soluci�
	 */
	void posarPecesElegant(char[][] Matriu) {
		char ascii = 65; // ASCII dec 65 = A equival a ascii='A';
		int comptador = 0;
		for (int F = 0; F < TAULER; F++)
			for (int C = 0; C < TAULER; C++) {
				if (comptador != 0 && (comptador % 2) == 0)
					ascii++;

				Matriu[F][C] = ascii;
				comptador++;
			}
	}

}
