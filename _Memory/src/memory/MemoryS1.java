/**
 * Memory Sessi� 1
 * Gener 2021 DAMvi1E
 * Objectius:
 * 		- Definir taulers (de joc i secret)
 * 		- Men� principal provisional i main per distribuir opcions
 * 		- Inicialitzar el tauler de joc
 * 		- Posar peces
 * 		- possibilitat de visualitzar els taulers
 **/

package memory;

import java.util.Scanner;

public class MemoryS1
{
	public static Scanner input = new Scanner(System.in);  //variable global, accesible a tot el codi
	public static final int TAULER = 4;					  //constant

	public static void main(String[] args)
	{
		char[][] Tauler = new char[TAULER][TAULER];		//variable local
		char[][] Secret = new char[TAULER][TAULER];		//variable local
		int Opcio;
		System.out.println("Benvinguts al joc MEMORY!");
		System.out.println();
		do
		{
			Opcio = Menu();
			switch (Opcio)
			{
			default:
			case 0:
				break;

			case 1:
				inicialitzarTauler(Tauler);
				break;

			case 2:
				mostrarTauler(Tauler);
				break;

			case 3:
				posarPeces(Tauler);
				break;
			}
		} while (Opcio != 0);
	}

	public static void inicialitzarTauler(char[][] Matriu)
	{
		for (int F = 0; F < TAULER; F++)
			for (int C = 0; C < TAULER; C++)
				Matriu[F][C] = '?';
	}

	public static void mostrarTauler(char[][] Matriu)
	{
		for (int F = 0; F < TAULER; F++)
		{
			System.out.print(F);
			for (int C = 0; C < TAULER; C++)
				System.out.print(" " + Matriu[F][C]);
			System.out.println();
		}
		System.out.print("  0 1 2 3");
		System.out.println();
	}

	public static void posarPeces(char[][] Matriu)
	{
		/*
		Matriu[0][0] = 'A';
		Matriu[0][1] = 'A';
		Matriu[0][2] = 66;  //'B'
		...  (raonable per ser 16 posicions)
		*/
		
		for (int F = 0; F < TAULER; F++)
			for (int C = 0; C < TAULER; C++)
				if (F == 0)
					if (C == 0 || C == 1)
						Matriu[F][C] = 'A';
					else
						Matriu[F][C] = 'B';
				else if (F == 1)
					if (C == 0 || C == 1)
						Matriu[F][C] = 'C';
					else
						Matriu[F][C] = 'D';
				else if (F == 2)
					if (C == 0 || C == 1)
						Matriu[F][C] = 'E';
					else
						Matriu[F][C] = 'F';
				else if (F == 3)
					if (C == 0 || C == 1)
						Matriu[F][C] = 'G';
					else
						Matriu[F][C] = 'H';
	}

	public static int Menu()
	{
		int Opcio;
		System.out.println("0. Sortir");
		System.out.println("1. Inicialitzar Tauler");
		System.out.println("2. Mostrar Tauler");
		System.out.println("3. Posar Peces");
		do
		{
			System.out.print("Digues una Opci�: ");
			Opcio = input.nextInt();
			if (Opcio > 3 || Opcio < 0)
				System.out.println("Opcio no v�lida");

		} while (Opcio > 3 || Opcio < 0);
		System.out.println();
		return Opcio;
	}
	
	void posarPecesElegant(char[][]Matriu) {
		char ascii = 65; //ASCII dec 65 = A  equival a   ascii='A';
		int comptador = 0;
		for (int F = 0; F < TAULER; F++)
			for (int C = 0; C < TAULER; C++)
			{
				if(comptador != 0 && (comptador%2) == 0)
					ascii++;

				Matriu[F][C] = ascii;
				comptador++;
				
			}
	}
}

