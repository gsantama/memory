/**
  * Memory Sessi� 1
 * Gener 2021 DAMvi1E
 * Objectius:
 * 		- Definir taulers (de joc i secret)
 * 		- Men� principal provisional i main per distribuir opcions
 * 		- Inicialitzar el tauler de joc
 * 		- Posar peces
 * 		- possibilitat de visualitzar els taulers
 * 
 * 	Objectius (Sessi� 2):
 * 		- fer les funcions 
 * 				remenarPeces
 * 				inicialitzarSecret
 * 				obtenirFila
 * 				obtenirColumna
 * 				validarCasella
 * 				destaparCasella
 *				taparCasella
 *				destapaCasella
 *				triaCasella	
 *		Afegir al men� provisional RemenarPeces, triaCasella (per destapar-la)
 **/

package memory;

import java.util.Scanner;

public class MemoryS2 {
	public static Scanner input = new Scanner(System.in);
	public static final int TAULER = 4;
	public static Scanner sc = new Scanner(System.in);

	public static void main(String[] args)
	{
		char[][] Tauler = new char[TAULER][TAULER];		//variable local
		char[][] Secret = new char[TAULER][TAULER];		//variable local
		int Opcio;
		System.out.println("Benvinguts al joc MEMORY!");
		System.out.println();
		do
		{
			Opcio = Menu();
			switch (Opcio)
			{
			default:
			case 0:
				break;

			case 1:
				inicialitzarTauler(Tauler);
				break;

			case 2:
				mostrarTauler(Tauler);
				break;
			case 3:
				posarPeces(Secret);
				break;
			case 4:
				remenarPeces(Secret);
				mostrarTauler(Secret);
				break;
			case 5:
				triaCasella(Tauler, Secret);
				break;
			}
		} while (Opcio != 0);
	}
	
	public static void inicialitzarTauler(char[][] Matriu) {
		for (int F = 0; F < TAULER; F++)
			for (int C = 0; C < TAULER; C++)
				Matriu[F][C] = '?';
	}

	public static void mostrarTauler(char[][] Matriu) {
		for (int F = 0; F < TAULER; F++) {
			System.out.print(F);
			for (int C = 0; C < TAULER; C++)
				System.out.print(" " + Matriu[F][C]);
			System.out.println();
		}
		System.out.print("  0 1 2 3");
		System.out.println();
	}

	public static void posarPeces(char[][] Matriu) {
		/*
		 * Matriu[0][0] = 'A'; Matriu[0][1] = 'A'; Matriu[0][2] = 66; //'B' ...
		 * (raonable per ser 16 posicions)
		 */

		for (int F = 0; F < TAULER; F++)
			for (int C = 0; C < TAULER; C++)
				if (F == 0)
					if (C == 0 || C == 1)
						Matriu[F][C] = 'A';
					else
						Matriu[F][C] = 'B';
				else if (F == 1)
					if (C == 0 || C == 1)
						Matriu[F][C] = 'C';
					else
						Matriu[F][C] = 'D';
				else if (F == 2)
					if (C == 0 || C == 1)
						Matriu[F][C] = 'E';
					else
						Matriu[F][C] = 'F';
				else if (F == 3)
					if (C == 0 || C == 1)
						Matriu[F][C] = 'G';
					else
						Matriu[F][C] = 'H';
	}

	public static void remenarPeces(char[][] Matriu) {
		int Fila, Columna;
		char Aux;

		for (int F = 0; F < TAULER; F++)
			for (int C = 0; C < TAULER; C++) {
				Fila = (int) (Math.random() * TAULER);
				Columna = (int) (Math.random() * TAULER);
				Aux = ' ';

				Aux = Matriu[F][C];
				Matriu[F][C] = Matriu[Fila][Columna];
				Matriu[Fila][Columna] = Aux;
			}
	}


	public static int Menu()
	{
		int Opcio;
		System.out.println("0. Sortir");
		System.out.println("1. Inicialitzar Tauler");
		System.out.println("2. Mostrar Tauler");
		System.out.println("3. Posar Peces");
		System.out.println("4. Remanar Peces");
		System.out.println("5. Tria Casella");
		
		do
		{
			System.out.print("Digues una Opci�: ");
			Opcio = input.nextInt();
			if (Opcio > 5 || Opcio < 0)
				System.out.println("Opcio no v�lida");

		} while (Opcio > 5 || Opcio < 0);
		System.out.println();
		return Opcio;
	}

	public static int obtenirFila () {
		int fila;
		// obtenir valor de coordenada fila
			do {
				System.out.println("Digem la fila que vols?");
				fila = sc.nextInt();
				if (fila < 0 || fila >= TAULER)
					System.out.println("Si us plau, entre 0 i " + (TAULER - 1));
			} while (fila < 0 || fila >= TAULER);
			return fila;
	};
	public static int obtenirColumna () {
			int columna;
			// obtenir valor de coordenada Y
			do {
				System.out.println("Digem la columna que vols?");
				columna = sc.nextInt();
				if (columna < 0 || columna >= TAULER)
					System.out.println("Si us plau, entre 0 i " + (TAULER - 1));
			} while (columna < 0 || columna >= TAULER);
			return columna;
	}
	
	public static boolean validaCasella(char[][]matriu, int fila, int columna) {
			return (matriu[fila][columna] == '?');
	}

	public static char obtenirCasella(char[][]matriu, int fila, int columna) {
		return (matriu[fila][columna]);
}

	public static void destapaCasella(char[][]matriu, int fila, int columna, char valor) {
		matriu[fila][columna]=valor;
	}
	
	public static void tapaCasella(char[][]matriu,int fila, int columna) {
		matriu[fila][columna]='?';
	}
	
	public static void triaCasella(char[][] tauler, char[][]secret) {
		int fila, columna;
		do {
			fila = obtenirFila();
			columna = obtenirColumna();
			if(!validaCasella(tauler,fila,columna)) 
				System.out.println("Casella ja destapada. Tria una altra");
		}while (!validaCasella(tauler,fila,columna));
		
		char valor = obtenirCasella(secret, fila, columna);
		destapaCasella(tauler,fila,columna,valor);	
	}
	

	void posarPecesElegant(char[][]Matriu) {
		char ascii = 65; //ASCII dec 65 = A  equival a   ascii='A';
		int comptador = 0;
		for (int F = 0; F < TAULER; F++)
			for (int C = 0; C < TAULER; C++)
			{
				if(comptador != 0 && (comptador%2) == 0)
					ascii++;

				Matriu[F][C] = ascii;
				comptador++;		
			}
	}

}
